import { readFileSync, writeFile, JFReadOptions, JFWriteOptions } from 'jsonfile'

interface Entry {
  id: string
}

interface Options {
  readOptions: JFReadOptions
  writeOptions: JFWriteOptions
}

export class DB<T extends Entry> {
  protected data!: T[]

  constructor(protected path: string, protected options: Partial<Options> = {}) {
    if (path.endsWith('.json')) {
      this.data = readFileSync(path, this.options.readOptions)
    } else {
      throw new Error('Only JSON files supported')
    }
  }

  get total (): number {
    return this.data.length
  }

  get all (): T[] {
    return [...this.data].reverse()
  }

  findOne<K extends keyof T> (key: K, value: T[K]): T | null {
    return this.all.find(o => o[key] === value) || null
  }

  find<K extends keyof T> (query: K, value: T[K]): T[] {
    return this.all.filter(o => o[query] === value)
  }

  getById (id: string): T | null {
    return this.findOne('id', id)
  }

  filter (callbackfn: (doc: T, index: number, arr: T[]) => boolean) {
    return this.all.filter(callbackfn)
  }

  paginate (page: number, itemsPerPage: number, items = this.all) {
    const start = (page - 1) * itemsPerPage
    const end = start + itemsPerPage
    return items.slice(start, end)
  }

  protected push (data: T) {
    if (this.findOne('id', data.id)) {
      throw new Error(`Id '${data.id}' already exists`)
    }
    this.data.push(data)
    return this.save()
  }

  save (): Promise<void> {
    return writeFile(this.path, this.data, this.options.writeOptions || {})
  }
}
