import { DB } from './jsondb'
import { JFReadOptions, JFWriteOptions } from 'jsonfile'
import slug from './slug'

interface EntryWithSlug {
  id: string
  slug: string
}

interface Options {
  readOptions: JFReadOptions
  writeOptions: JFWriteOptions
}

type slugTransformer = {
  slugTransformer: (text: string) => string
}

export class DB_WITH_SLUG<T extends EntryWithSlug> extends DB<T> {
  constructor(protected path: string, protected options: Partial<Options & slugTransformer>) {
    super(path, options)
  }

  findBySlug (slug: string) {
    return this.findOne('slug', slug)
  }

  push (data: T) {
    if (this.findOne('id', data.id)) {
      throw new Error(`An entry with Id '${data.id}' already exists`)
    }
    if (this.findOne('slug', data.slug)) {
      throw new Error(`An entry with Slug '${data.slug}' already exists`)
    }
    return super.push(data)
  }

  getUniqueSlug (text: string): string {
    const s = (this.options.slugTransformer || slug)(text)
    let c = 1;
    let result = s;
    while (!this.findOne('slug', result)) {
      result = `${s}_${++c}`
    }
    return result
  }
}