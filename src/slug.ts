export default function slug (text: string) {
  return text.toLowerCase()
    .replace(/&/u, 'and')
    .replace(/[\.,\?']/g, '').replace(/[^a-z0-9-]/g, '-')
    .replace(/-+/g, '-')
    .replace(/^-/, '').replace(/-$/, '')
}
